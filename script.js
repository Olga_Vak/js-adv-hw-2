class Hamburger {
    constructor(size, stuffing){
        try {
            if (size.name === 'small' || size.name === 'large') {
                this._size = size;
            } else {
                throw new HamburgerException('Укажите размер гамбургера!')
            }
        } catch (e) {
            console.log(e.massage);
        }
        try{
            if (stuffing.name === 'cheese'|| stuffing.name === 'potato' || stuffing.name === 'salad'){
                this._stuffing = stuffing;
            } else {
                throw new HamburgerException('Укажите добавку для гамбургера!')
            }
        } catch (e) {
            console.log(e.massage);
        }

        this._topping = [];
    }

    addTopping(topping){
       try {
           if (topping.name === 'spice' || topping.name === 'mayo') {
               let isExist = this._topping.find(function(item){
                   return topping.name === item.name;
               });
               if (isExist === undefined){
                   this._topping.push(topping);
                   return this._topping;
               } else {
                   return console.log('такая добавка уже есть')
               }
           }
       }catch (err) {
           console.log('Нет такой добавки')
       }
    }

    removeTopping(topping){
        try {
            let isExist = this._topping.indexOf(topping);
            if (isExist >=0){
                this._topping.splice(isExist,1);
                return this._topping;
            }
        }catch (err) {
            console.log('топпинг не найден')
        }
    }

    calculatePrice() {
        let toppingPrice = function () {
            let toppingPrice = 0;
            for (let value of this._topping) {
                return toppingPrice += +value.price;
            }
        };
        let totalPrice = 0;
        return totalPrice + this._size.price + this._stuffing.price + toppingPrice.call(this);
    }

    calculateCalories(){
        let toppingCalories = function(){
            let toppingCalories = 0;
            for (let value of this._topping){
                return toppingCalories += +value.calorie;
            }
        };
        let totalCalories = 0;
        return totalCalories + this._size.calorie + this._stuffing.calorie + toppingCalories.call(this);
    }


    get size() {
        return this._size;
    }


    set size(value) {
        this._size = value;
    }


    get stuffing() {
        return this._stuffing;
    }


    set stuffing(value) {
        this._stuffing = value;
    }

    get topping() {
        return this._topping;
    }

    set topping(value) {
        this._topping = value;
    }
}

Hamburger.SIZE_SMALL = {
    name: 'small',
    type: 'size',
    price: 50,
    calorie: 20,
};
Hamburger.SIZE_LARGE = {
    name: 'large',
    type: 'size',
    price: 100,
    calorie: 40,
};
Hamburger.STUFFING_CHEESE = {
    name: 'cheese',
    type: 'stuffing',
    price: 10,
    calorie: 20,
};
Hamburger.STUFFING_SALAD = {
    name: 'salad',
    type: 'stuffing',
    price: 20,
    calorie: 5,
};
Hamburger.STUFFING_POTATO = {
    name: 'potato',
    type: 'stuffing',
    price: 15,
    calorie: 10,
};
Hamburger.TOPPING_MAYO = {
    name: 'mayo',
    type: 'topping',
    price: 20,
    calorie: 5,
};
Hamburger.TOPPING_SPICE = {
    name: 'spice',
    type: 'topping',
    price: 15,
    calorie: 0,
};

function HamburgerException (massage) {
    this.massage = massage;
}

// маленький гамбургер с начинкой из сыра
const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.size === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.topping.length); // 1

// не передали обязательные параметры
const h2 = new Hamburger(); // => HamburgerException: no size given

// передаем некорректные значения, добавку вместо размера
const h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
const h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// HamburgerException: duplicate topping 'TOPPING_MAYO'
